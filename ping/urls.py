from django.urls import path

from . import views

app_name = 'ping'
urlpatterns = [
    path('network/', views.index, name='index'),
    path('network/pressed/', views.pressed, name='pressed'),
    path('network/update/', views.update, name='update'),
    path('network/agents/', views.agents, name='agents'),
    path('network/agent/kill', views.kill, name='kill'),
    path('network/<int:client_id>/activate/', views.client_activate, name='active'),
    path('experiment/new/', views.entry, name='entry'),
    path('experiment/', views.experiment, name='experiment'),
    path('experiment/<int:experiment_id>/check/', views.check, name='check'),
    path('experiment/<int:experiment_id>/select/', views.select, name='select'),
    path('experiment/<int:experiment_id>/delete/', views.delete, name='delete'),
    path('play/', views.main_play, name='play'),
    path('play/scheduler/start', views.scheduler_start, name='start'),
    path('play/scheduler/step', views.scheduler_step, name='step'),
    path('play/scheduler/stop', views.scheduler_stop, name='stop'),
    path('play/global/exit', views.experiment_stop, name='exit'),
    path('play/global/share', views.experiment_share, name='share'),
    path('play/global/start', views.experiment_start, name='start_exp'),
    path('play/global/load', views.experiment_load, name='load_exp'),
    path('play/global/kill', views.experiment_kill, name='kill_exp'),
    path('play/metric/<int:metrics_id>/', views.experiment_metric, name='metrics'),
    path('play/export_csv/', views.export_csv, name='csv'),
]