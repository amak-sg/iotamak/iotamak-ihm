from django.contrib import admin

from .models import *

admin.site.register(Client)
admin.site.register(Agent)
admin.site.register(Experiment)
admin.site.register(Metrics)
admin.site.register(CurrentExperiment)
admin.site.register(Network)
admin.site.register(Link)
admin.site.register(Node)