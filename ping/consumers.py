from channels.generic.websocket import AsyncWebsocketConsumer


import json


class GraphConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.room_name = 'event'
        self.room_group_name = self.room_name+"_graph"
        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )
        print(self.room_group_name)
        await self.accept()

    async def disconnect(self, code):
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )
        print("DISCONNECED CODE: ",code)

    async def receive(self, text_data=None, bytes_data=None):
        data = json.loads(text_data)
        message = data['message']
        await self.channel_layer.group_send(
            self.room_group_name,{
                "type": 'send_message_to_frontend',
                "message": message
            }
        )
    async def send_message_to_frontend(self,event):
        # Receive message from room group
        message = event['message']
        # Send message to WebSocket
        await self.send(text_data=json.dumps({
            'message': message
        }))


class CanvasConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.room_name = 'event'
        self.room_group_name = self.room_name+"_canvas"
        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )
        print(self.room_group_name)
        await self.accept()

    async def disconnect(self, code):
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )
        print("DISCONNECED CODE: ",code)

    async def receive(self, text_data=None, bytes_data=None):
        data = json.loads(text_data)
        message = data['message']
        await self.channel_layer.group_send(
            self.room_group_name,{
                "type": 'send_message_to_frontend',
                "message": message
            }
        )
    async def send_message_to_frontend(self,event):
        # Receive message from room group
        message = event['message']
        # Send message to WebSocket
        await self.send(text_data=json.dumps({
            'message': message
        }))


class ComConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.room_name = 'event'
        self.room_group_name = self.room_name+"_com"
        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )
        print(self.room_group_name)
        await self.accept()

    async def disconnect(self, code):
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )
        print("DISCONNECED CODE: ",code)

    async def receive(self, text_data=None, bytes_data=None):
        data = json.loads(text_data)
        message = data['message']
        await self.channel_layer.group_send(
            self.room_group_name,{
                "type": 'send_message_to_frontend',
                "message": message
            }
        )
    async def send_message_to_frontend(self,event):
        # Receive message from room group
        message = event['message']
        # Send message to WebSocket
        await self.send(text_data=json.dumps({
            'message': message
        }))