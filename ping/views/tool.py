import os
import shutil

from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from iotAmak.tool.remote_client import RemoteClient
from iotAmak.tool.ssh_client import SSHClient

from ping.models import Client, Network


def delete_folder(folder_name):
    for filename in os.listdir(folder_name):
        file_path = os.path.join(folder_name, filename)
        try:
            if os.path.isfile(file_path) or os.path.islink(file_path):
                os.unlink(file_path)
            elif os.path.isdir(file_path):
                shutil.rmtree(file_path)
        except Exception as e:
            print('Failed to delete %s. Reason: %s' % (file_path, e))


def get_remote_client():
    res = []
    for client in Client.objects.all():
        if client.status == "Online" and client.active:
            res.append(RemoteClient(client.hostname, client.username, client.password))

    return res


def get_ssh_client():
    n = Network.objects.all()[0]
    return SSHClient(get_remote_client(), n.path_to_iotamak)


def canvas_event_triger(metrics):
    channel_layer = get_channel_layer()
    async_to_sync(channel_layer.group_send)(
        'event_canvas',
        {
            'type': 'send_message_to_frontend',
            'message': metrics
        }
    )

def com_event_triger(data):
    channel_layer = get_channel_layer()
    async_to_sync(channel_layer.group_send)(
        'event_com',
        {
            'type': 'send_message_to_frontend',
            'message': data
        }
    )

def graph_event_triger(data):
    channel_layer = get_channel_layer()
    async_to_sync(channel_layer.group_send)(
        'event_graph',
        {
            'type': 'send_message_to_frontend',
            'message': data
        }
    )