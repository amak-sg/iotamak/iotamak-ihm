import os

from django.conf import settings

from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from django.urls import reverse

from .experiment_checker import unzip, contains_basic_files, check_config
from .tool import delete_folder
from ..models import Experiment, ExperimentForm


def entry(request):
    if request.method == 'POST':
        form = ExperimentForm(request.POST, request.FILES)

        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('ping:experiment'))

    else:
        form = ExperimentForm()
        template = loader.get_template('ping/entry.html')
        context = {"form": form}
        return HttpResponse(template.render(context, request))


def check(request, experiment_id):
    exp = Experiment.objects.get(pk=experiment_id)

    if not unzip(str(settings.MEDIA_ROOT) + str(exp.media), str(settings.MEDIA_ROOT) + "/tmp", exp):
        return HttpResponseRedirect(reverse('ping:experiment'))

    folder_path = str(settings.MEDIA_ROOT) + "/tmp/" + exp.name

    if not contains_basic_files(folder_path, exp):
        delete_folder(str(settings.MEDIA_ROOT) + "/tmp")
        return HttpResponseRedirect(reverse('ping:experiment'))

    if not check_config(folder_path, exp):
        delete_folder(str(settings.MEDIA_ROOT) + "/tmp")
        return HttpResponseRedirect(reverse('ping:experiment'))

    exp.status = "Checked"
    exp.save()
    delete_folder(str(settings.MEDIA_ROOT) + "/tmp")
    return HttpResponseRedirect(reverse('ping:experiment'))

def select(request, experiment_id):

    exp = Experiment.objects.get(pk=experiment_id)

    if exp.status == "Checked":
        exp_list = Experiment.objects.filter(status="Selected")
        for expe in exp_list:
            expe.status = "Checked"
            expe.save()
        exp.status = "Selected"
        exp.save()
    return HttpResponseRedirect(reverse('ping:experiment'))

def delete(request, experiment_id):
    exp = Experiment.objects.get(pk=experiment_id)
    os.remove(str(settings.MEDIA_ROOT) + str(exp.media))

    Experiment.objects.get(pk=experiment_id).delete()
    return HttpResponseRedirect(reverse('ping:experiment'))

def experiment(request):
    template = loader.get_template('ping/experiment.html')
    context = {
        "experiments": Experiment.objects.all(),
    }
    return HttpResponse(template.render(context, request))
